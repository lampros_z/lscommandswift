protocol FileSystemEntry{
    var name: String{ get }
    var size: Float{ get }
    var level: Int{ get }
}