import Foundation

let args = CommandLine.arguments

let url = URL(fileURLWithPath: args[1], isDirectory: true)

let appLaunchFolder = Folder(name: url.relativePath, level: 0)

print("Working directory:[\(appLaunchFolder.name)]")

Parser.parse(folder: appLaunchFolder)
print(appLaunchFolder)